@extends('index.pub.base')
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 欢迎使用，laravelflow 工作流插件示例</nav>
<div class="page-container">
    <h2>如果对您有帮助，您可以点击 💘Star💘支持</h2>
    <article class="page-404 minWP text-c">
        <p class="error-title"><i class="Hui-iconfont va-m">&#xe68e;</i>
            <span class="va-m"> LaravelFlow</span>
        </p>
        <p class="error-description">很高兴，在此与您相遇~</p>
        <p class="error-info">您可以：
            <a href="https://gitee.com/ntdgg/tpflow" target="_blank"  class="c-primary">&lt; 下载</a>
            <span class="ml-20">|</span>
            <a href="https://gadmin8.com/index/product.html" target="_blank"  class="c-primary ml-20">文档 </a>
            <span class="ml-20">|</span>
            <a href="https://www.cojz8.com/topic/2" target="_blank" class="c-primary ml-20">授权 &gt;</a>
        </p>
        <p class="error-info">版本：<span class="label badge-primary radius">V1.0.0</span> 正式版</p>
    </article>
    <div style="padding: 0px 48px;font-size: x-large;">
        <b>全新发布 </b>
        <ul>
            <li>
                Gadmin企业级极速开发框架：
                <a href="http://demo.gadmin8.com" target="_blank"  class="c-primary"><span class="label label-primary radius">演示</span></a>
                <span class="ml-20">|</span>
                <a href="https://gadmin8.com/index/product.html" target="_blank"  class="c-primary ml-20"><span class="label label-success  radius">文档</span> </a>
                <span class="ml-20">|</span>
                <a href="https://gadmin8.com/index/Page/index.html?cate=14" target="_blank" class="c-primary ml-20"><span class="label label-danger radius">授权</span> </a>
            </li>
            <li>
                SFDP超级表单开发系统：
                <a href="http://sfdp.gadmin8.com" target="_blank"  class="c-primary"><span class="label label-primary radius">演示</span></a>
                <span class="ml-20">|</span>
                <a href="https://gadmin8.com/index/product.html" target="_blank"  class="c-primary ml-20"><span class="label label-success  radius">文档</span> </a>

            </li>
            <li>
                Tpflow授权申请：
                <a href="https://market.topthink.com/product/244" target="_blank"  class="c-primary"><span class="label label-danger  radius">购买</span></a>
            </li>
        </ul>
        <div class="Huialert Huialert-error" style="background-color: #000000;border-color: #f9f6f6;"><i class="Hui-iconfont">&#xe6a6;</i>
            Tip:使用LaravelFlow必须遵循国家相关法律法规且遵守MIT开源协议。您可以用于商业系统，但请保留版权，否则将视为侵权。
        </div>
    </div>
</body>
</html>
